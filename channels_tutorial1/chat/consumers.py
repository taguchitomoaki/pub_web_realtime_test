from channels.generic.websocket import WebsocketConsumer
import json

import cv2
import numpy as np 
from PIL import Image
import io
import sys
import base64

class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        self.send(text_data=json.dumps({
            'message': message
        }))

class VideoConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, bytes_data, text_data=None):
        video_bytes = bytes_data

        arr = np.asarray(bytearray(video_bytes), dtype=np.uint8)
        # img = cv2.imdecode(arr, -1)
        img = cv2.imdecode(arr, cv2.IMREAD_ANYCOLOR)
        # テストとしての二値化
        gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
        ret, th2 = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
        #retval, buf = cv2.imencode('.jpg', img)
        img_base64 = base64.b64encode(th2)
        print(img.flatten()[0:10])
        print(img_base64, 'img_base64')
        print(type(img_base64), 'type(buf)')
        #print(img_base64.shape, 'buf.shape')

        self.send(bytes_data=img_base64)

        cv2.imshow('img', img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
       


